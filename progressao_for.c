/* Faça um programa que receba do usuário a quantidade de elementos de uma Progressão Aritmética (PA) e a razão,
gere uma PA em lista com a quantidade de elementos definido pelo usuário e imprima. Uma PA tem como primeiro
elemento o número 0 e o próximo elemento é o anterior somado da razão.

Exemplo de uma PA com razão 3:
0, 3, 6, 9, 12...*/

#include <stdio.h>

int main()
{   
    /*Criando variaveis que vão receber a quantidade de elementos, razão, o contador para a quantidade de elementos
    e para raceber os dados da progressão*/
    int quant, razao;
    int contador; 
    int prog = 0;

    //recebendo e lendo a quantidade de elementos desejada pelo usuário
    printf("digite a quantidade de elementos da progressao: ");
    scanf("%d", &quant);

    //recebendo e lendo a razão desejada pelo usuário
    printf("digite a razao da progressao: ");
    scanf("%d", &razao);

    //toda progressão aritmética começa pelo numero 0
    printf("0");

    //criando um for para para contar a quantidade de elementos e para fazer a adição da razão
    for(contador = 2; contador <= quant; contador++)
    {
        prog = prog + razao;
        printf("%d", prog);
    }
return 0;
}