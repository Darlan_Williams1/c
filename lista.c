/* Crie um Menu. O menu deverá ter opções quaisquer de 1 a 9, se o usuário digitar algum número entre 1 e 9,o programa deverá reexibir o menu.
Caso o usuário digitar qualquer outro número, com exceção do 0, o programa deve informar que a opção desejada é inexistente.
E por fim, caso o usuário digitar 0, o programa deve exibir mensagem de despedida e terminar sua execução. */

#include <stdio.h>

int main()
{
    //criando variavel para receber o valor do iten desejado pelo usuario, inicializada com o valor 1, para garantir pelo menos uma repetição
    int iten = 1;

    //estrutura condicional em que se o numero recebido pela variavel iten for diferente de 0, o menu vai ser impresso denovo
    while(iten != 0)
    {    
        //recebendo e armazenando o numero do iten desejado pelo usuario
        printf("\n1-feijao \n2-arroz \n3-macarrao \n4-batata \n5-batata doce \n6-carne \n7-ovo \n8-uva \n9-tomate \nescolha pelo numero o seu iten:");
        scanf("%d", &iten);

        //estrutura condicional que depende do numero do iten informado pelo usuario para imprimir na tela
        switch (iten)
        {
            case 0:
                printf("Voce finalizou suas compras, obg!");
                break;
                return 0; 
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                printf("Voce escolheu: %d", iten);
                break;
            //Se um numero acima de 9 for recebido, o switch imprime o default
            default:
                printf("Opcao inexistente!");
                break;
        }

    }
return 0;
}