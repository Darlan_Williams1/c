#include <stdio.h>
#include <string.h>

int main()
{
    char nome1[15];
    char nome2[15];
    int cond = 1;

    while (cond == 1)
    {
        printf("Digite um nome: ");
        scanf("%s", nome1);

        printf("Digite outro nome: ");
        scanf("%s", nome2);

        strcat(nome1, nome2);

        printf("nome concatenado = %s\n", nome1);

        printf("voce deseja copiar o valor de outra string? 1 = (sim) 0 = (nao) ");
        scanf("%d", &cond);
    }
    return 0;
}