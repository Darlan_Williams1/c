/* Elabore um programa em linguagem C que receba um ano (numérico inteiro) e informe se o ano é bissexto ou não
(anos bissextos são múltiplos de 4, portanto, se a divisão do ano por 4 gerar resto igual a zero, o ano é bissexto - use o operador %). */

#include <stdio.h>

int main()
{
    //Criando variavel para receber do usuário o valor do ano
    int ano;

    //Lendo e guardando o valor do ano
    printf("Digite um ano para verificar se ele e bissexto: ");
    scanf("%d", &ano);

    /*estrutura condicional, se o resto da divisão do ano por 4 for 0, o ano é bissexto
    caso contrario, o ano não é bissexto*/
    if((ano % 4) == 0)
    {
        printf("O ano e bissexto!");
    }
    else
    {
        printf("O ano nao e bissexto");
    }
return 0;
}