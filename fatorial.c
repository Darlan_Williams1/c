/* Faça um programa que leia um número e retorne o fatorial deste número.*/

#include <stdio.h>

int main()
{
    //Criando uma variavel para receber o valor do usuario e uma para receber o valor do fatorial
    int num, fatorial = 1;

    //recebendo o valor da variavel e guardando na variavel
    printf("Digite um numero para verificarmos o fatorial: ");
    scanf("%d", &num);

    /*Criando um for para fazer a repetição das multiplicações fatoriais de acordo com o valor dado pelo
    usuario(num)*/
    for(; num > 1; --num)
    {
        fatorial = (num * fatorial);
    }
    printf("o fatorial e %d", fatorial);

}