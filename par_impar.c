/*Faça um programa que leia um número e diga se esse número é par ou não*/

#include <stdio.h>

int main()
{
    //criando variavel para recber o numero desejado pelo usuario
    int numero;

    //Recebendo e lendo o numero informado pelo usuario
    printf("Digite um numero: ");
    scanf("%d", &numero);

    /*Criando um estrutura condicional onde se o resto da divisão do numero por 2 for 0
    o numero é par, caso contario, é impar*/
    if((numero % 2) == 0)
    {
        printf("O numero %d e par!", numero);
    } else
    {
        printf("O numero %d e impar!", numero);
    }
return 0;
}